from django.conf.urls import url

from .views import file_pcap_list_create, file_pcap_detail




urlpatterns = [
    url(r'^$', file_pcap_list_create),
    url(r'^(?P<file_pcap_id>[0-9]+)/$', file_pcap_detail)

    
]