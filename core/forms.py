from django.forms import ModelForm

from .models import FilePcap 

class FilePcapForm(ModelForm):
    class Meta:
        model = FilePcap
        fields = ['name', 'upload' ]
    