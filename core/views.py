from django.shortcuts import render, redirect
from django.http import HttpResponse    

from .models import FilePcap
from .forms import FilePcapForm

import pyshark

def file_pcap_list_create(request):
    file_pcap_list = FilePcap.objects.all()
    if request.method == 'POST':
        form = FilePcapForm(request.POST, request.FILES)
        if form.is_valid():
            print ('OK')
            file_pcap = form.save()
            return redirect('http://localhost:8000/')
    else:
        form = FilePcapForm()

    return render(request, 'file_pcap_list_create.html', {
        'file_pcap_list': file_pcap_list,
        'form': form
    })

def file_pcap_detail(request, file_pcap_id):
    file_pcap = FilePcap.objects.get(id = file_pcap_id)

    file_pcap_path = (file_pcap.upload.path)
    cap = pyshark.FileCapture(file_pcap_path)
    print (cap[13])
    import json
    json_cap  = json.dumps(cap[13].eth.__dict__,  sort_keys=True, indent=4)
    
    print (json_cap)


    return render(request, 'file_pcap_detail.html', {
        'file_pcap': file_pcap    
    })




