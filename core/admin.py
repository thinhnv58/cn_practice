from django.contrib import admin

from .models import FilePcap

admin.site.register(FilePcap)